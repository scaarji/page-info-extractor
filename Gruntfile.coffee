module.exports = (grunt) ->
    grunt.initConfig
        pkg: grunt.file.readJSON('package.json')
        mochaTest:
            test:
                options:
                    reporter: 'spec'
                    require: 'coffee-script/register'
                src: ['test/**/test_*.coffee']

        coffeelint:
            app: [
                'lib/**/*.coffee'
            ]
            options:
                no_tabs:
                    level: 'error'
                max_line_length:
                    value: 80,
                    level: 'error'
                camel_case_classes:
                    level: 'error'
                indentation:
                    value: 2,
                    level: 'error'
                no_throwing_strings:
                    level: 'error'
                cyclomatic_complexity:
                    value: 11,
                    level: 'ignore'
                line_endings:
                    value: 'unix',
                    level: 'ignore'
                no_implicit_parens:
                    level: 'ignore'
                no_stand_alone_at:
                    level: 'ignore'
        env:
            test:
                NODE_ENV: 'test'
        githooks:
            all:
                'pre-commit': 'test'

    grunt.loadNpmTasks('grunt-mocha-test')
    grunt.loadNpmTasks('grunt-coffeelint')
    grunt.loadNpmTasks('grunt-githooks')

    grunt.registerTask 'test', ['coffeelint', 'mochaTest']
    grunt.registerTask 'default', ['githooks', 'test']
