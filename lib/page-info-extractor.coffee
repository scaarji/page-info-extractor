async = require 'async'
$ = require 'cheerio'

DEFAULT_PARSERS =
  title: (dom, options, cb) ->
    if typeof options is 'function'
      cb = options
      options = {}

    selectors = [
      '[itemscope][itemtype="http://schema.org/Article"] [itemprop="name"]'
      'meta[property="og:title"]'
      'meta[name="og:title"]'
      'head > title'
      'h1'
    ]
    for selector in selectors when (tag = dom.find(selector)).length
      tag = tag.first()
      return cb null, tag.attr('content') or tag.text()
    cb null, ''

  description: (dom, options, cb) ->
    if typeof options is 'function'
      cb = options
      options = {}

    selectors = [
      'meta[property="og:description"]'
      'meta[name="og:description"]'
      'head > description'
    ]
    for selector in selectors when (tag = dom.find(selector)).length
      tag = tag.first()
      return cb null, (tag.attr('content') or tag.text()).trim()
    cb null, ''

  image: (dom, options, cb) ->
    if typeof options is 'function'
      cb = options
      options = {}

    maxDepth = options.maxDepth or 2
    host = options.host or ''
    minWidth = options.minWidth or 0
    minHeight = options.minHeight or 0
    selectors = [
      '[itemscope][itemtype="http://schema.org/Article"] [itemprop="image"]'
      'meta[property="og:image"]'
      'meta[name="og:image"]'
      'link[rel="image_src"]'
    ]
    images = []
    for selector in selectors
      tag = dom.find(selector)
      if tag.length
        images = [].concat images, tag.attr('content') or tag.attr('href') or
        tag.text()

    container = dom.find('h1')
    # if page contains more than one h1 tag - we can no be sure which one
    # represents article title
    if container.length is 1
      level = 0
      while container.length and level <= maxDepth
        imgTags = container.find('img')
        if imgTags.length
          imgTags.each ->
            $this = $(this)
            return if (_size = $this.attr('height')) and _size < minHeight
            return if (_size = $this.attr('width')) and _size < minWidth
            images.push $this.attr('src')
          break

        container = container.parent()
        level += 1

    images =
      for image in images
        if /^\/[^\/]/.test image
          host + image
        else
          image

    if images.length > 1
      # remove duplicates
      hash = {}
      images =
        for image in images when hash[image] isnt yes
          hash[image] = yes
          image

    switch images.length
      when 0 then cb null, null
      when 1 then cb null, images.shift()
      else cb null, images

  url: (dom, options, cb) ->
    if typeof options is 'function'
      cb = options
      options = {}

    selectors = [
      'meta[property="og:url"]'
      'meta[name="og:url"]'
    ]
    for selector in selectors when (tag = dom.find(selector)).length
      tag = tag.first()
      return cb null, tag.attr('content')
    cb null, null

class Extractor
  constructor: ->
    @_parsers = {}
    # clone default parsers
    @_parsers[field] = parser for own field, parser of DEFAULT_PARSERS

    @extract = @extract.bind(this)

  addParser: (name, fn) ->
    if typeof name isnt 'string'
      throw new TypeError 'Name should be string'

    if typeof fn isnt 'function'
      throw new TypeError 'Parser body should be function'

    @_parsers[name] = fn
    this

  extract: (html, options = {}, cb) ->
    if typeof options is 'function'
      cb = options
      options = {}

    if typeof html isnt 'string'
      return cb new TypeError 'html should be string'

    dom = $(html)
    res = {}
    fields =
      if options.fields?.length and options.fields instanceof Array
        (field for field in options.fields when field of @_parsers)
      else
        (field for own field of @_parsers)

    async.reduce fields, {}, (res, field, cb) =>
      async.waterfall [
        (cb) =>
          @_parsers[field](dom, options[field] or {}, (err, value) ->
            cb err, value
          )
        (value, cb) ->
          res[field] = value
          cb null, res
      ], cb
    , cb

module.exports = new Extractor
