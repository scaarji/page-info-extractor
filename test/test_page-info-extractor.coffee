async = require 'async'
should = require 'should'
sinon = require 'sinon'

$ = require 'cheerio'

pageInfoExtractor = require '../lib/page-info-extractor'

describe 'page-info-extractor', ->
  extractor = null
  beforeEach ->
    extractor = new pageInfoExtractor.constructor
  describe 'parsers', ->
    describe 'title', ->
      it 'should grab info from microdata if possible', (done) ->
        extractor._parsers.title $("""
          <!doctype html>
          <html lang="en">
          <head>
            <meta charset="UTF-8">
            <title>Document</title>
          </head>
          <body>
            <div itemscope="" itemtype="http://schema.org/Article">
              <span itemprop="name">Page title</span>
              <span itemprop="image">image.jpg</span>
              <span itemprop="thumbnailUrl">thumbnail.jpg</span>
              <span itemprop="datePublished">2014-03-31T14:36:31+03:00</span>
            </div>
          </body>
          </html>
          """), (err, title) ->
          title.should.eql 'Page title'
          done err

      it 'should grab info from og:title meta tag', (done) ->
        async.series [
          (cb) ->
            extractor._parsers.title $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta property="og:title" content="Page title">
              </head>
              <body>

              </body>
              </html>
              """), (err, title) ->
                title.should.eql 'Page title'
                cb err

          (cb) ->
            extractor._parsers.title $("""
            <!doctype html>
            <html lang="en">
            <head>
              <meta charset="UTF-8">
              <title>Document</title>
              <meta property="og:title" content="Some other page title">
            </head>
            <body>

            </body>
            </html>
            """), (err, title) ->
              title.should.eql 'Some other page title'
              cb err

          (cb) ->
            extractor._parsers.title $("""
            <!doctype html>
            <html lang="en">
            <head>
              <meta charset="UTF-8">
              <title>Document</title>
              <meta name="og:title" content="Some other page title">
            </head>
            <body>

            </body>
            </html>
            """), (err, title) ->
              title.should.eql 'Some other page title'
              cb err
        ], done

      it 'should grab info from title tag if og:title is not present', (done) ->
        async.series [
          (cb) ->
            extractor._parsers.title $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Title tag content</title>
              </head>
              <body>

              </body>
              </html>
              """), (err, title) ->
                title.should.eql 'Title tag content'
                cb err
          (cb) ->
            extractor._parsers.title $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Some other random title tag content</title>
              </head>
              <body>

              </body>
              </html>
              """), (err, title) ->
                title.should.eql 'Some other random title tag content'
                cb err
        ], done

      it 'should grab info from h1 tag if neither og:title nor title tags are \
      preset', (done) ->
        async.series [
          (cb) ->
            extractor._parsers.title $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
              </head>
              <body>
                <article>
                  <title>
                    <h1>Page title in h1</h1>
                  </title>
                </article>
              </body>
              </html>
              """), (err, title) ->
                title.should.eql 'Page title in h1'
                cb err
          (cb) ->
            extractor._parsers.title $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
              </head>
              <body>
                <small>I stand in your way</small>
                <h1>Some other page title in other h1</h1>
                <h1>I am not title by the way</h1>
              </body>
              </html>
              """), (err, title) ->
                title.should.eql 'Some other page title in other h1'
                cb err
        ], done

      it 'should return empty string if nothing found', (done) ->
        async.series [
          (cb) ->
            extractor._parsers.title $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
              </head>
              <body>

              </body>
              </html>
              """), (err, title) ->
                title.should.eql ''
                cb err

          (cb) ->
            extractor._parsers.title $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
              </head>
              <body>

              </body>
              </html>
              """), (err, title) ->
                title.should.eql ''
                cb err
        ], done

    describe 'description', ->
      it 'should grab info from og:description meta tag', (done) ->
        async.series [
          (cb) ->
            extractor._parsers.description $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta property="og:description" content="Page description">
              </head>
              <body>

              </body>
              </html>
              """), (err, description) ->
                description.should.eql 'Page description'
                cb err

          (cb) ->
            extractor._parsers.description $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta property="og:description"
                  content="Some other page description">
              </head>
              <body>

              </body>
              </html>
              """), (err, description) ->
                description.should.eql 'Some other page description'
                cb err

          (cb) ->
            extractor._parsers.description $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta name="og:description"
                  content="Yet another page description">
              </head>
              <body>

              </body>
              </html>
              """), (err, description) ->
                description.should.eql 'Yet another page description'
                cb err
        ], done

      it 'should grab info from description meta tag if og:description is not \
      available', (done) ->
        async.series [
          (cb) ->
            extractor._parsers.description $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <description>Title tag content</description>
              </head>
              <body>

              </body>
              </html>
              """), (err, description) ->
                description.should.eql 'Title tag content'
                cb err
          (cb) ->
            extractor._parsers.description $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <description>
                  Some other random description tag content
                </description>
              </head>
              <body>

              </body>
              </html>
              """), (err, description) ->
                description.should.eql('Some other random description tag \
                  content')
                cb err
        ], done

    describe 'image', ->
      it 'should grab image(-s) from microdata, og:image and image_src meta \
      tags', (done) ->
        async.series [
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
              </head>
              <body>
                <div itemscope="" itemtype="http://schema.org/Article">
                  <span itemprop="name">Page title</span>
                  <span itemprop="image">image.jpg</span>
                  <span itemprop="thumbnailUrl">thumbnail.jpg</span>
                  <span itemprop="datePublished">
                    2014-03-31T14:36:31+03:00
                  </span>
                </div>
              </body>
              </html>
              """), (err, image) ->
                image.should.eql 'image.jpg'
                cb err
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta property="og:image" content="path-to-image.jpg">
              </head>
              <body>

              </body>
              </html>
              """), (err, image) ->
                image.should.eql 'path-to-image.jpg'
                cb err
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta name="og:image" content="path-to-image-1.png">
                <meta property="og:image" content="path-to-image-2.gif">
              </head>
              <body>

              </body>
              </html>
              """), (err, image) ->
                image.sort().should.eql [
                  'path-to-image-1.png'
                  'path-to-image-2.gif'
                ].sort()
                cb err
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta name="og:image" content="path-to-image-1.png">
                <meta property="og:image" content="path-to-image-2.gif">
                <link rel="image_src" href="path-to-image-3.jpg">
              </head>
              <body>

              </body>
              </html>
              """), (err, image) ->
                image.sort().should.eql [
                  'path-to-image-1.png'
                  'path-to-image-2.gif'
                  'path-to-image-3.jpg'
                ].sort()
                cb err
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta name="og:image" content="path-to-other-image.png">
              </head>
              <body>

              </body>
              </html>
              """), (err, image) ->
                image.should.eql 'path-to-other-image.png'
                cb err
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <link rel="image_src" href="path-to-other-image.png">
              </head>
              <body>

              </body>
              </html>
              """), (err, image) ->
                image.should.eql 'path-to-other-image.png'
                cb err
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta name="og:image" content="path-to-other-image.png">
                <link rel="image_src" href="path-to-other-image.png">
              </head>
              <body>

              </body>
              </html>
              """), (err, image) ->
                image.should.eql 'path-to-other-image.png'
                cb err
        ], done
      it 'should return null if not images found', (done) ->
        extractor._parsers.image $("""
        <!doctype html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <title>Document</title>
          <meta property="og:title" content="Page title">
        </head>
        <body>

        </body>
        </html>
        """), (err, image) ->
          should.not.exist image
          done err
      it 'should try to grab image close to h1 tag if it is available',
      (done) ->
        async.series [
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
              </head>
              <body>
                <div class="article">
                  <h1>Article title</h1>
                  <div class="annotation">
                    <img src="path-to-image.jpg" alt="">
                  </div>
                </div>
              </body>
              </html>
              """), (err, image) ->
                image.should.eql 'path-to-image.jpg'
                cb err
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
              </head>
              <body>
                <div class="article">
                  <header>
                    <h1>Article title</h1>
                  </header>
                  <div class="annotation">
                    <img src="path-to-image.jpg" alt="">
                  </div>
                </div>
              </body>
              </html>
              """), (err, image) ->
                image.should.eql 'path-to-image.jpg'
                cb err
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
              </head>
              <body>
                <article>
                  <header>
                    <h1>Article title</h1>
                    <div class="annotation">
                      <img src="path-to-image.jpg" alt="">
                    </div>
                  </header>
                  <img src="other-image-path.jpg" />
                </article>
              </body>
              </html>
              """), (err, image) ->
                image.should.eql 'path-to-image.jpg'
                cb err
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
              </head>
              <body>
                <article>
                  <header>
                    <h1>Article title</h1>
                    <div class="annotation">
                        <img src="path-to-image.jpg" alt="">
                        <img src="path-to-other-image.jpg" alt="">
                      </div>
                  </header>
                  <img src="other-image-path.jpg" />
                </article>
              </body>
              </html>
              """), (err, image) ->
                image.should.eql [
                  'path-to-image.jpg'
                  'path-to-other-image.jpg'
                ]
                cb err
          (cb) ->
            extractor._parsers.image $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
              </head>
              <body>
                <div class="article">
                  <header>
                    <h1>Article title</h1>
                  </header>
                </div>
                <div class="annotation">
                  <img src="path-to-image.jpg" alt="">
                </div>
              </body>
              </html>
              """), (err, image) ->
                should.not.exist image
                cb err
        ], done
      it 'should not grab images from page body if more then one h1 tag found \
      on page', (done) ->
        extractor._parsers.image $("""
        <!doctype html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <title>Document</title>
        </head>
        <body>
          <header>
            <h1>Page header</h1>
          </header>
          <div class="article">
            <h1>Article title</h1>
            <div class="annotation"><img src="path-to-image.jpg" alt=""></div>
          </div>
        </body>
        </html>
        """), (err, image) ->
          should.not.exist image
          done err
      it 'should grab images from all sources', (done) ->
        extractor._parsers.image $("""
        <!doctype html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <title>Document</title>
          <meta property="og:image" content="path-to-image-1.jpg">
          <link rel="image_src" href="path-to-image-2.jpg">
        </head>
        <body>
          <article>
            <header>
              <h1>Article title</h1>
              <div class="annotation">
                  <img src="path-to-image-3.jpg" alt="">
                  <img src="path-to-image-4.jpg" alt="">
                </div>
            </header>
            <img src="other-image-path.jpg" />
          </article>
        </body>
        </html>
        """), (err, image) ->
          image.should.eql [
            'path-to-image-1.jpg'
            'path-to-image-2.jpg'
            'path-to-image-3.jpg'
            'path-to-image-4.jpg'
          ]
          done err
      it 'should append host to image sources if relative path is given',
      (done) ->
        host = 'http://example.com'
        img = '/path-to-image-3.jpg'
        extractor._parsers.image $("""
        <!doctype html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <title>Document</title>
        </head>
        <body>
          <article>
            <header>
              <h1>Article title</h1>
              <div class="annotation">
                  <img src="#{img}" alt="">
                </div>
            </header>
          </article>
        </body>
        </html>
        """), host: host, (err, image) ->
          image.should.eql(host + img)
          done err
      it 'should filter out small images if limits are set', (done) ->
        extractor._parsers.image $("""
        <!doctype html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <title>Document</title>
        </head>
        <body>
          <article>
            <header>
              <h1>Article title</h1>
              <div class="annotation">
                  <img src="image-1.jpg" alt="" height="10" width="2312">
                  <img src="image-2.jpg" alt="" height="60" width="2312">
                  <img src="image-3.jpg" alt="">
                </div>
            </header>
          </article>
        </body>
        </html>
        """), minWidth: 50, minHeight: 50, (err, image) ->
          image.should.eql [
            'image-2.jpg'
            'image-3.jpg'
          ]
          done err

    describe 'url', ->
      it 'should grab url from og:url', (done) ->
        async.series [
          (cb) ->
            extractor._parsers.url $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta property="og:url" content="Page url">
              </head>
              <body>

              </body>
              </html>
              """), (err, url) ->
                url.should.eql 'Page url'
                cb err

          (cb) ->
            extractor._parsers.url $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta property="og:url"
                  content="Some other page url">
              </head>
              <body>

              </body>
              </html>
              """), (err, url) ->
                url.should.eql 'Some other page url'
                cb err
          (cb) ->
            extractor._parsers.url $("""
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta name="og:url"
                  content="Yet another page url">
              </head>
              <body>

              </body>
              </html>
              """), (err, url) ->
                url.should.eql 'Yet another page url'
                cb err
        ], done

      it 'should return null if url is not found', (done) ->
        extractor._parsers.url $("""
        <!doctype html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <title>Document</title>
        </head>
        <body>

        </body>
        </html>
        """), (err, url) ->
          should.not.exist url
          done err

  describe 'main', ->
    describe '#addParser', ->
      it 'should throw unless name is set and parser is function', ->
        (extractor.addParser.bind(null, 123, 'asd')).should.throw()
        (extractor.addParser.bind(null, 'asdasd', 'asd')).should.throw()
        (extractor.addParser.bind(null, 123, (->))).should.throw()
        (extractor.addParser.bind(null, null, (->))).should.throw()
        (extractor.addParser.bind(null, 'asdasd', null)).should.throw()
      it 'should add parser to set', ->
        name = 'unknown parser'
        fn = ->
        extractor._parsers.should.not.have.property name
        extractor.addParser(name, fn)
        extractor._parsers.should.have.property name, fn

      it 'should override existing parser', ->
        name = 'unknown parser'
        fn1 = ->
        extractor.addParser(name, fn1)
        extractor._parsers.should.have.property name, fn1
        fn2 = ->
        extractor.addParser(name, fn2)
        extractor._parsers.should.have.property name, fn2

    describe '#extract', ->
      it 'should return error unless html if given', (done) ->
        extractor.extract null, (err) ->
          should.exist err
          done()

      it 'should call all required parsers or all parser if fields param is \
      not set', (done) ->
        tasks = [
          {
            fields: ['title', 'description']
            body: """
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <description>Description</description>
              </head>
              <body>

              </body>
              </html>
              """
            res:
              title: 'Document'
              description: 'Description'
          }
          {
            fields: ['title', 'image']
            body: """
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <description>Description</description>
                <meta property="og:image" content="image-path.jpg">
              </head>
              <body>

              </body>
              </html>
              """
            res:
              title: 'Document'
              image: 'image-path.jpg'
          }
          {
            fields: null
            body: """
              <!doctype html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <meta name="og:title" content="Page title">
                <description>Page description</description>
                <meta property="og:image" content="image-path.jpg">
              </head>
              <body>

              </body>
              </html>
              """
            res:
              title: 'Page title'
              description: 'Page description'
              image: 'image-path.jpg'
              url: null
          }
        ]
        spies = {}
        for own field of extractor._parsers
          spies[field] = sinon.spy extractor._parsers, field

        async.eachSeries tasks, (task, cb) ->
          extractor.extract task.body, fields: task.fields, (err, res) ->
            should.not.exist err

            for own field, spy of spies
              if not task.fields or field in task.fields
                spy.calledOnce.should.eql yes
              else
                spy.called.should.eql no
              spy.reset()

            res.should.eql task.res

            cb null
        , done

      it 'should pass options to parser according to name', (done) ->
        options =
          fields: ['image', 'title']
          image:
            host: 'host'
          title:
            foo: 'bar'
            bar: 'baz'

        imageSpy = sinon.spy extractor._parsers, 'image'
        titleSpy = sinon.spy extractor._parsers, 'title'

        extractor.extract """
        <!doctype html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <title>Document</title>
          <description>Description</description>
        </head>
        <body>

        </body>
        </html>
        """, options, (err, res) ->
          extractor._parsers.image.calledOnce.should.eql yes
          extractor._parsers.image.firstCall.args[1].should.eql options.image
          extractor._parsers.title.calledOnce.should.eql yes
          extractor._parsers.title.firstCall.args[1].should.eql options.title

          done err
